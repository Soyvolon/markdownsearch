﻿namespace MarkdownSearch.Data.Search
{
    /// <summary>
    /// Configurable options for searching files.
    /// </summary>
    public class SearchOptions
    {
        /// <summary>
        /// When a matching string is found, what heading level should be used to act as
        /// a grouping to return results for.
        /// </summary>
        public int HeadingGroupDepth { get; set; } = 2;

        /// <summary>
        /// If true, the search engine will try and find
        /// all search terms in a document before looking
        /// for actual matches.
        /// </summary>
        public bool MatchAll { get; set; } = false;
    }
}
