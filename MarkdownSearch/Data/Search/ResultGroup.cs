﻿using System;

namespace MarkdownSearch.Data.Search
{
    /// <summary>
    /// A group text that holds the search term.
    /// </summary>
    public class ResultGroup
    {
        /// <summary>
        /// The number of times this group was matched.
        /// </summary>
        public int Matches { get; set; } = 0;

        private string _relLink = string.Empty;
        /// <summary>
        /// The relative link to the header that contains this content.
        /// </summary>
        public string RelativeLink
        {
            get => _relLink;
            set => _relLink = value.StartsWith('/') ? value[1..] : value;
        }

        /// <summary>
        /// The absolute URI to the header.
        /// </summary>
        public Uri? Link { get; set; }

        /// <summary>
        /// The title for this result group.
        /// </summary>
        public string Title { get; set; } = "";

        /// <summary>
        /// The markdown contents of this group.
        /// </summary>
        public string Contents { get; set; } = "";
    }
}
