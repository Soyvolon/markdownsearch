﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace MarkdownSearch.Data.Search
{
    /// <summary>
    /// The result of a markdown text search.
    /// </summary>
    public class SearchResult : IEnumerable<ResultGroup>
    {
        /// <summary>
        /// The results for a search.
        /// </summary>
        public IReadOnlyList<ResultGroup> Results { get; }

        /// <summary>
        /// Create a new instance of the results.
        /// </summary>
        /// <param name="results">The results for a search.</param>
        public SearchResult(List<ResultGroup> results)
        {
            Results = results.OrderByDescending(e => e.Matches)
                .ToList();
        }

        /// <summary>
        /// Gets the enumerator for <see cref="Results"/>.
        /// </summary>
        /// <returns>The enumerator for <see cref="Results"/></returns>
        public IEnumerator<ResultGroup> GetEnumerator()
            => Results.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator()
            => GetEnumerator();
    }
}
