﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MarkdownSearch.Data.Engine
{
    /// <summary>
    /// A class to represent a markdown header.
    /// </summary>
    public class MarkdownHeader
    {
        /// <summary>
        /// The parent header for this header. Used for backwards navigation.
        /// </summary>
        public MarkdownHeader? ParentHeader { get; set; } = null;
        /// <summary>
        /// The headers this header has underneath it.
        /// </summary>
        public List<MarkdownHeader> ChildHeaders { get; set; } = new List<MarkdownHeader>();
        /// <summary>
        /// The value after the #s for this header.
        /// </summary>
        public string Name { get; set; } = string.Empty;
        /// <summary>
        /// The depth of this header (how many #s it has).
        /// </summary>
        public int Depth { get; set; } = -1;
        /// <summary>
        /// The starting index of this header in the file.
        /// </summary>
        public int Start { get; set; } = 0;
        /// <summary>
        /// The ending index of this header. Or null, if the end
        /// is the end of the file.
        /// </summary>
        public int? End { get; set; } = null;

        /// <summary>
        /// Populates the ending indexes with the start index of the
        /// previous header.
        /// </summary>
        public void PopulateEndingIndexes(int? end = null)
        {
            if (Depth < 0)
            {
                // don't do anything for the top level header.
                ChildHeaders.ForEach(e => e.PopulateEndingIndexes(end));
                return;
            }

            int? rolling = end;
            foreach (var item in ChildHeaders.AsEnumerable().Reverse())
            {
                item.PopulateEndingIndexes(rolling);
                rolling = item.Start;
            }

            End = ChildHeaders.LastOrDefault()?.End ?? end;
        }

        /// <summary>
        /// Gets a header object that is at the <paramref name="wantedDepth"/>.
        /// </summary>
        /// <param name="name">The name of the header to look for.</param>
        /// <param name="depth">The depth the header is at.</param>
        /// <param name="wantedDepth">The header needed. Must be less than <paramref name="depth"/>.</param>
        /// <returns>The first instance of the requested header if it exists.</returns>
        public MarkdownHeader? GetHeaderAtDepthFor(string name, int depth, int wantedDepth)
        {
            if (depth > wantedDepth)
                throw new ArgumentException($"{nameof(wantedDepth)} must be smaller than {nameof(depth)}",
                    nameof(wantedDepth));

            var header = GetHeader(name, depth);

            if (header is null)
                return null;

            for (int i = 0; i < depth - wantedDepth; i++)
            {
                header = header?.ParentHeader;
                if (header is null)
                    return null;
            }

            return header;
        }

        /// <summary>
        /// Gets a header.
        /// </summary>
        /// <param name="name">The name of the header.</param>
        /// <param name="depth">The depth the header is at.</param>
        /// <returns>The first instance of this header if it exists.</returns>
        public MarkdownHeader? GetHeader(string name, int depth)
        {
            if (Depth == depth)
                return Name == name ? this : null;

            foreach (var c in ChildHeaders)
            {
                var res = c.GetHeader(name, depth);
                if (!(res is null))
                    return res;
            }

            return null;
        }
    }
}
