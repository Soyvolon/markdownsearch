﻿using MarkdownSearch.Data.Search;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarkdownSearch.Data.Engine
{
    /// <summary>
    /// A class to parse a folder of markdown files.
    /// </summary>
    public class FolderEngine<TPage> : IDisposable
        where TPage : PageEngine
    {
        private bool _initialized = false;
        private bool disposedValue;

        /// <summary>
        /// The root path this folder is at.
        /// </summary>
        public string RootPath { get; set; }

        /// <summary>
        /// The files to parse.
        /// </summary>
        public List<TPage> Pages { get; set; } = new List<TPage>();
        /// <summary>
        /// The child folders to parse files in.
        /// </summary>
        public List<FolderEngine<TPage>> Folders { get; set; } = new List<FolderEngine<TPage>>();

        /// <summary>
        /// Creates a new instance of the <see cref="FolderEngine{TPage}"/>
        /// </summary>
        /// <param name="path">A path to an existing folder.</param>
        public FolderEngine(string path)
        {
            RootPath = path;
        }

        /// <summary>
        /// Gets the folders who's parent folder is this object.
        /// </summary>
        /// <returns>The child folders.</returns>
        protected virtual Task<string[]> GetChildFoldersAsync()
        {
            return Task.FromResult(Directory.GetDirectories(RootPath));
        }

        /// <summary>
        /// Gets all pages in this folder.
        /// </summary>
        /// <returns>The pages that are a part of this folder.</returns>
        protected virtual Task<string[]> GetChildPagesAsync()
        {
            return Task.FromResult(Directory.GetFiles(RootPath));
        }

        /// <summary>
        /// Initializes the engine and all subordinate engines.
        /// </summary>
        /// <remarks>
        /// As this is recursive, it only needs to be called on the
        /// top level <see cref="FolderEngine{TPage}"/>. It will auto
        /// generate engines for an existing file tree.
        /// </remarks>
        /// <param name="searchRoot">The root search path.</param>
        /// <param name="prefix">The prefix for links in this folder structure.</param>
        /// <param name="folderParams">The additional folder parameters.</param>
        /// <param name="pageParams">The additional page parameters.</param>
        public async Task InitializeAsync(string searchRoot, string prefix,
            object?[]? folderParams = null, object?[]? pageParams = null)
        {
            // Register
            foreach (var dir in await GetChildFoldersAsync())
            {
                var parameters = new List<object?>()
                {
                    dir
                };

                if (!(folderParams is null))
                    parameters.AddRange(folderParams);

                var engine = (FolderEngine<TPage>)Activator.CreateInstance(GetType(), parameters.ToArray());
                Folders.Add(engine);
            }

            foreach (var file in await GetChildPagesAsync())
            {
                var parameters = new List<object?>()
                {
                    file, searchRoot, prefix
                };

                if (!(pageParams is null))
                    parameters.AddRange(pageParams);

                var page = (TPage)Activator.CreateInstance(typeof(TPage), parameters.ToArray());
                Pages.Add(page);
            }

            // Initialize
            foreach (var folder in Folders)
                await folder.InitializeAsync(searchRoot, prefix, folderParams, pageParams);

            foreach (var file in Pages)
                await file.InitializeAsync();

            _initialized = true;
        }

        /// <summary>
        /// Searches all child files and folders for the search terms.
        /// </summary>
        /// <param name="searchTerms">The terms to search by.</param>
        /// <param name="options">The search options.</param>
        /// <param name="searchRegex">The regex to use for searching.</param>
        /// <returns>An <see cref="List{T}"/> of <see cref="ResultGroup"/>s that contains
        /// the found results.</returns>
        public async Task<List<ResultGroup>> SearchAsync(string[] searchTerms, SearchOptions options, Regex searchRegex)
        {
            if (!_initialized)
                throw new InvalidOperationException($"This method can not be called until " +
                    $"after {nameof(InitializeAsync)} has been called.");

            var results = new List<ResultGroup>();
            foreach (var file in Pages)
            {
                if (!file.HasKeyword(options.MatchAll, searchTerms))
                    continue;

                var group = await file.GetResultGroups(options, searchRegex);
                results.AddRange(group.Values);
            }

            foreach (var folder in Folders)
            {
                var res = await folder.SearchAsync(searchTerms, options, searchRegex);
                results.AddRange(res);
            }

            return results;
        }

        /// <summary>
        /// Internal dispose method.
        /// </summary>
        /// <param name="disposing">True if called from <see cref="Dispose()"/></param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (var folder in Folders)
                        folder.Dispose();

                    foreach (var page in Pages)
                        page.Dispose();
                }

                Pages = null!;
                Folders = null!;
                disposedValue = true;
            }
        }

        /// <summary>
        /// Dispose this engine.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
