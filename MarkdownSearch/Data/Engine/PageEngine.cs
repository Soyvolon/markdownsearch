﻿using MarkdownSearch.Data.Search;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarkdownSearch.Data.Engine
{

    /// <summary>
    /// A class to parse markdown files and return results.
    /// </summary>
    public partial class PageEngine : IDisposable
    {
        private static readonly TimeSpan Timeout = TimeSpan.FromSeconds(2);

        private bool _initialized = false;
        private bool disposedValue;

        /// <summary>
        /// The path of the file this engine is responsible for.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// The path used for linking to this file.
        /// </summary>
        public string LinkRoot { get; set; }

        /// <summary>
        /// The root header for this object.
        /// </summary>
        public MarkdownHeader RootHeader { get; protected set; } = new MarkdownHeader();

        /// <summary>
        /// The various words used in this document.
        /// </summary>
        public HashSet<string> Keywords { get; protected set; } = new HashSet<string>();

        /// <summary>
        /// Creates a new file parsing engine.
        /// </summary>
        /// <param name="path">The path of the data to parse.</param>
        /// <param name="searchRoot">The path of the root folder for this search.</param>
        /// <param name="prefix">The prefix for the links in this search.</param>
        public PageEngine(string path, string searchRoot, string prefix)
        {
            Path = path;
            LinkRoot = System.IO.Path.Join(prefix, System.IO.Path.GetRelativePath(searchRoot, path));
        }

        /// <summary>
        /// Gets the contents of the data at the specified <see cref="Path"/>.
        /// </summary>
        /// <returns>The file contents.</returns>
        protected virtual async Task<string> GetContentsAsync()
            => await File.ReadAllTextAsync(Path);

        /// <summary>
        /// Initializes this file engine for processing a single file.
        /// </summary>
        public async Task InitializeAsync()
        {
            var res = await GetContentsAsync();

            Keywords = new HashSet<string>(LargeWordsRegex
                .Split(res)
                .Where(e => !string.IsNullOrWhiteSpace(e))
                .Select(e => e.ToLower()));

            var matches = HeadingsRegex.Matches(res);

            var parents = new Stack<MarkdownHeader>();
            parents.Push(RootHeader);
            foreach (Match match in matches)
            {
                var parent = parents.Peek();
                var val = match.Value.Trim();

                var d = match.Groups.ElementAtOrDefault(1)?.Value.Trim().Length ?? -1;

                if (d == -1)
                    throw new Exception("Failed to capture header depth.");

                var elem = new MarkdownHeader()
                {
                    Name = val[d..].Trim(),
                    Depth = d,
                    ParentHeader = parent,
                    Start = match.Index
                };

                if (d <= parent.Depth)
                {
                    var diff = parent.Depth - d;
                    for (int i = 0; i <= diff; i++)
                        _ = parents.Pop();

                    parent = parents.Peek();
                }

                if (d > parent.Depth)
                {
                    parents.Push(elem);
                }

                parent.ChildHeaders.Add(elem);
            }

            RootHeader.PopulateEndingIndexes();

            _initialized = true;
        }

        /// <summary>
        /// Check the keywords and see if they contain the relevant words.
        /// </summary>
        /// <param name="matchAll">if true, all words in searchWords must match.</param>
        /// <param name="searchTerms">The search terms. Multi-word terms will be broken
        /// down into single world expressions.</param>
        /// <returns>True if the keywords are present.</returns>
        public bool HasKeyword(bool matchAll, params string[] searchTerms)
        {
            var search = searchTerms.SelectMany(e => OnlyWordsRegex.Split(e))
                .Where(e => !string.IsNullOrWhiteSpace(e));

            if (matchAll)
                return search.All(Keywords.Contains);
            else return search.Any(Keywords.Contains);
        }

        /// <summary>
        /// Searches the file for matches in the regex.
        /// </summary>
        /// <param name="options">The options to use.</param>
        /// <param name="searchRegex">The search regex to use.</param>
        /// <returns>A dictionary of <see cref="ResultGroup"/>s for the found elements in
        /// the search. The keys are the header value for the group.</returns>
        public async Task<Dictionary<string, ResultGroup>> GetResultGroups(SearchOptions options, Regex searchRegex)
        {
            if (!_initialized)
                throw new InvalidOperationException($"This method can not be called until " +
                    $"after {nameof(InitializeAsync)} has been called.");

            var file = await GetContentsAsync();
            var matches = searchRegex.Matches(file);

            Dictionary<string, ResultGroup> results = new Dictionary<string, ResultGroup>();
            foreach (Match match in matches)
            {
                var key = match.Groups.ElementAtOrDefault(1)?.Value;

                if (key is null)
                    continue;

                var depth = match.Groups.ElementAtOrDefault(2)?.Length ?? 0;
                var title = match.Groups.ElementAtOrDefault(3)?.Value.Trim() ?? "";

                var titleLink = LinkRoot + '#' + string.Join('-', OnlyWordsRegex.Split(title.ToLower()));
                titleLink = titleLink.Replace('\\', '/');

                if (!results.TryGetValue(titleLink, out var group))
                {
                    bool useStartToStartMatch = depth > options.HeadingGroupDepth;

                    MarkdownHeader? header;
                    if (!useStartToStartMatch)
                        header = RootHeader.GetHeaderAtDepthFor(title, depth, options.HeadingGroupDepth);
                    else header = RootHeader.GetHeader(title, depth);

                    if (header is null)
                        // the header wasn't found so lets silently fail in case this was
                        // intentional.
                        return new Dictionary<string, ResultGroup>();

                    var start = header.Start;
                    var end = (useStartToStartMatch
                        ? header.ChildHeaders.FirstOrDefault()?.Start
                        : header.End) ?? file.Length;

                    group = new ResultGroup()
                    {
                        Contents = file[start..end],
                        RelativeLink = titleLink,
                        Title = title,
                    };

                    results.Add(titleLink, group);
                }

                group.Matches++;
            }

            return results;
        }

        private static readonly Regex HeadingsRegex = new Regex("^(#+).*$", RegexOptions.Multiline, Timeout);
        private static readonly Regex LargeWordsRegex = new Regex("\\W+(?>\\b\\w{0,2}\\b)?", RegexOptions.Multiline, Timeout);
        private static readonly Regex OnlyWordsRegex = new Regex("\\W+", RegexOptions.Multiline, Timeout);

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {

                }

                RootHeader = null!;
                Keywords = null!;
                disposedValue = true;
            }
        }

        /// <summary>
        /// Dispose this engine.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}