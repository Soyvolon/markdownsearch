﻿using MarkdownSearch.Data.Engine;
using MarkdownSearch.Data.Search;

using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MarkdownSearch.Services
{
    /// <summary>
    /// A service for searching markdown files.
    /// </summary>
    public interface IMarkdownSearchService<TFolder, TPage> : IDisposable
        where TFolder : FolderEngine<TPage>
        where TPage : PageEngine
    {
        /// <summary>
        /// Sets the link start.
        /// </summary>
        /// <param name="linkStart">The value that should be appended to the start of every link.</param>
        public void SetLinkStart(string linkStart);

        /// <summary>
        /// Tells the service what objects to append to the
        /// constructors for folders after the parameters
        /// required for <see cref="FolderEngine{TPage}"/>
        /// are passed.
        /// </summary>
        /// <remarks>
        /// Only applicable if a custom implementation of <see cref="FolderEngine{TPage}"/>
        /// is used.
        /// </remarks>
        /// <param name="paramObjs">The objects to use as parameters.</param>
        public void SetFolderParams(params object?[] paramObjs);

        /// <summary>
        /// Tells the service what objects to append to the
        /// constructors for folders after the parameters
        /// required for <see cref="PageEngine"/>
        /// are passed.
        /// </summary>
        /// <remarks>
        /// Only applicable if a custom implementation of <see cref="PageEngine"/>
        /// is used.
        /// </remarks>
        /// <param name="paramObjs">The objects to use as parameters.</param>
        public void SetPageParams(params object?[] paramObjs);

        /// <summary>
        /// Searches a list of folders for a string of search terms
        /// </summary>
        /// <param name="folders">The folders to search for .md files in.</param>
        /// <param name="searchTerms">The terms to search by.</param>
        /// <returns>A <see cref="Task{T}"/> with a <see cref="SearchResult"/> containing the
        /// found data for the provided terms.</returns>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, params string[] searchTerms);

        /// <summary>
        /// Searches a list of folders for a string of search terms
        /// </summary>
        /// <param name="folders">The folders to search for .md files in.</param>
        /// <param name="prefixes">The prefixes for the navigation links on a per folder basis.
        /// There should be an equal number of elements in <paramref name="folders"/> as there is <paramref name="prefixes"/>.</param>
        /// <param name="searchTerms">The terms to search by.</param>
        /// <returns>A <see cref="Task{T}"/> with a <see cref="SearchResult"/> containing the
        /// found data for the provided terms.</returns>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, IEnumerable<string> prefixes, params string[] searchTerms);

        /// <summary>
        /// Searches a list of folders for a string of search terms.
        /// </summary>
        /// <param name="folders">The folders to search for .md files in.</param>
        /// <param name="options">The options to search using.</param>
        /// <param name="searchTerms">The terms to search by.</param>
        /// <returns>A <see cref="Task{T}"/> with a <see cref="SearchResult"/> containing the
        /// found data for the provided terms.</returns>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, SearchOptions options, params string[] searchTerms);

        /// <summary>
        /// Searches a list of folders for a string of search terms
        /// </summary>
        /// <param name="folders">The folders to search for .md files in.</param>
        /// <param name="prefixes">The prefixes for the navigation links on a per folder basis.
        /// There should be an equal number of elements in <paramref name="folders"/> as there is <paramref name="prefixes"/>.</param>
        /// <param name="searchTerms">The terms to search by.</param>
        /// <param name="options">The options to search using.</param>
        /// <returns>A <see cref="Task{T}"/> with a <see cref="SearchResult"/> containing the
        /// found data for the provided terms.</returns>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, IEnumerable<string> prefixes, 
            SearchOptions options, params string[] searchTerms);
    }
}
