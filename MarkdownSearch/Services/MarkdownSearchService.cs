﻿using MarkdownSearch.Data.Engine;
using MarkdownSearch.Data.Search;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MarkdownSearch.Services
{
    /// <inheritdoc/>
    public partial class MarkdownSearchService<TFolder, TPage> : IMarkdownSearchService<TFolder, TPage>
        where TFolder : FolderEngine<TPage>
        where TPage : PageEngine
    {
        private const string RegexSearchJoin = "[^#]*?";

        private static readonly TimeSpan Timeout = TimeSpan.FromSeconds(2);
        private static readonly Regex BadCharsRegex = new Regex("[^\\w\\.@-]", RegexOptions.Multiline, Timeout);

        private bool disposedValue;

        /// <summary>
        /// Folder engine cache.
        /// </summary>
        protected ConcurrentDictionary<string, TFolder> TopLevelEngines { get; set; }
            = new ConcurrentDictionary<string, TFolder>();

        /// <summary>
        /// A value that is appended to the start of every link.
        /// </summary>
        protected Uri? LinkStart { get; set; }
        /// <summary>
        /// The params to append to the folder constructors.
        /// </summary>
        protected object?[] FolderParams { get; set; } = new object[0];
        /// <summary>
        /// The params to append to the page constructors.
        /// </summary>
        protected object?[] PageParams { get; set; } = new object[0];

        /// <inheritdoc/>
        public void SetLinkStart(string linkStart)
        {
            var link = linkStart.EndsWith('/') ? linkStart : linkStart + '/';
            LinkStart = new Uri(link);
        }

        /// <inheritdoc/>
        public void SetFolderParams(params object?[] paramObjs)
            => FolderParams = paramObjs;

        /// <inheritdoc/>
        public void SetPageParams(params object?[] paramObjs)
            => PageParams = paramObjs;

        /// <inheritdoc/>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, params string[] searchTerms)
            => SearchFolders(folders, Enumerable.Repeat("", folders.Count()), new SearchOptions(), searchTerms);

        /// <inheritdoc/>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, SearchOptions options, params string[] searchTerms)
            => SearchFolders(folders, Enumerable.Repeat("", folders.Count()), new SearchOptions(), searchTerms);

        /// <inheritdoc/>
        public Task<SearchResult> SearchFolders(IEnumerable<string> folders, IEnumerable<string> prefixes, params string[] searchTerms)
            => SearchFolders(folders, prefixes, new SearchOptions(), searchTerms);

        /// <inheritdoc/>
        public async Task<SearchResult> SearchFolders(IEnumerable<string> folders, IEnumerable<string> prefixes,
            SearchOptions options, params string[] searchTerms)
        {
            var regexSearchPart = RegexSearchJoin + string.Join(RegexSearchJoin,
                searchTerms.Select(e => BadCharsRegex.Replace(e, "")));
            var regex = new Regex("((#+)\\s?(.*?))\\n(?>" + regexSearchPart + ")",
                RegexOptions.IgnoreCase, Timeout);

            var resultGroups = new List<ResultGroup>();

            var prefixEnumerator = prefixes.GetEnumerator();
            foreach (var folder in folders)
            {
                prefixEnumerator.MoveNext();
                var prefix = prefixEnumerator.Current;

                if (!TopLevelEngines.TryGetValue(folder, out var engine))
                {
                    var parameters = new List<object?>()
                    {
                        folder
                    };

                    parameters.AddRange(FolderParams);

                    engine = (TFolder)Activator.CreateInstance(typeof(TFolder), parameters.ToArray());
                    await engine.InitializeAsync(folder, prefix, FolderParams, PageParams);
                    TopLevelEngines[folder] = engine;
                }

                var res = await engine.SearchAsync(searchTerms, options, regex);
                resultGroups.AddRange(res);
            }

            if (!(LinkStart is null))
                resultGroups.ForEach(e => e.Link = new Uri(LinkStart, e.RelativeLink));
            return new SearchResult(resultGroups);
        }

        /// <summary>
        /// Internal dispose method.
        /// </summary>
        /// <param name="disposing">If true, actively called from the dispose method.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    foreach (var item in TopLevelEngines)
                        item.Value.Dispose();
                }

                TopLevelEngines = null!;
                disposedValue = true;
            }
        }

        /// <summary>
        /// Dispose this service.
        /// </summary>
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in 'Dispose(bool disposing)' method
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
