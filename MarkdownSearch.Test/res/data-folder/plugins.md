<!-- docs/plugins.md -->

# Plugins

There are a few plugins that have been added to the documentation
site that allow for some interactions and customization
of the documents.

**Added plugins:**

- [Docsify Tabs](https://jhildenbiddle.github.io/docsify-tabs/#/?id=usage)
- [File Name Replace](https://gitlab.com/Soyvolon/file-name-replace/-/blob/main/README.md?ref_type=heads)
- [Enhanced Command Line Blocks](https://gitlab.com/Soyvolon/enhanced-command-line-blocks/-/blob/main/README.md?ref_type=heads#page-and-local-overrides)
- [Variables Plugin](https://github.com/bandorko/docsify-variables?tab=readme-ov-file#usage)
- [Runkit](https://jhildenbiddle.github.io/docsify-plugin-runkit/#/?id=usage)

There are a few more plugins, but those are mainly for configuration and not
usage in writing documentation.

Please view the documentation for each plugin to get information on how
to use them.
