<!-- docs/_sidebar.md -->
<!-- markdownlint-disable MD041 Sidebars dont have titles -->

- [Docs](/docs/)
  - [Contributing](/docs/CONTRIBUTING.md)
  - [Git](/docs/git.md)
  - [Markdown](/docs/markdown.md)
  - [Plugins](/docs/plugins.md)
  - [Advanced](/docs/advanced.md)
