<!-- docs/README.md -->

# Documentation Meta Docs

> Documentation about documentation!

## Contributing

For information on contributing to FullSail Partners' documentation,
please visit the [Contributing](/docs/CONTRIBUTING.md) page.

Looking for a refresher on how to utilize `git` for this repo?
Check out the [Git](/docs/git.md) page.

## Resources

- The [FullSail Partners Documentation Repository](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation).
