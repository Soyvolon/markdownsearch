<!-- docs/advanced.md -->

# Advanced

There are a few things that you can do to make the documentation
better if you have the know how. These usually involve knowing
HTML, how to configure plugins, and/or CSS.

## HTML in Markdown

Markdown is a language that gets converted to HTML before it is
displayed. This means that any HTML you write directly into your
markdown document stays unchanged when rendered.

And as such, valid HTML in your markdown can be used to
configure plugins.

## Themeable

Themeable is a plugin that sets the theme of the website.
This specific plugin is configured to use only variables
that have default values, meaning any part of the theme can
be easily changed (ie: colors).

You can find more information on their documentation page
here: [Themeable](https://jhildenbiddle.github.io/docsify-themeable/#/introduction)
