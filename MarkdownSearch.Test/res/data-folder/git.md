<!-- docs/git.md -->
<!-- cmd:defaultLang:batch -->
<!-- cmd:defaults:data-prompt="C:\..\FSPDocumentation>" -->

# Git Basics

> The basic things needed for making changes to
> FullSail Partners Documentation.

## Terms

**Repository** - a place to store files. Or: "a central location in
which data is stored and managed."

## Installing Git

You can find the most recent version of git from their
[downloads page](https://git-scm.com/downloads).

The downloads page should display the version needed for your
computer in the blue screen on the right side of the page.
If you need a different version of git, you can
click one of the links found on their downloads page.

Run the installer once it is downloaded. Follow the prompts,
keeping the default options for them. If you notice any
settings (such as the default editor) that you wish to change,
feel free to do so, but the standard git settings are good for
the vast majority of use cases.

Once installed, you need to run two setup commands in a command
line. Open `PowerShell`, `Command Prompt`, or `Git Bash`. All
three are capable of running the following commands.

```cmd
<!-- data-prompt="C:\Users\Aria>" -->
git config --global user.name "Your Name"
git config --global user.email "Your Email"
```

Replace `Your Name` and `Your Email` with your name and email
respectively. Keep the quotes.

To verify you ran these commands correctly, you can run the
following commands to check the configured name and email.

```cmd
<!-- data-prompt="C:\Users\Aria>" -->
git config --global user.name
git config --global user.email
```

## Pulling A Repository

To *pull a repository* is to take a copy of it from the internet
and download it to your local computer, with a few extra steps
that allows you to easily keep your local copy up to date.

<!-- tabs:start -->

### **vscode**

1. At the [azure repository](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation),
under `Repos > Files > FSPDocumentation`, select the `Clone` button in
the top right corner.
2. Click `Clone in VS Code`.
3. Your browser will prompt you to open visual studio code. Hit `Open`.
4. Select the destination folder for your repo (where it will be saved).

If `Clone in VS Code` does not work, you can follow these steps instead:

1. Copy the `HTTPS` link.
2. In VS Code, open a new window.
3. Go to the source control menu (`ctrl+shift+g`).
4. Click `Clone Repository`.
5. Paste the link in the box that appears at the top of your screen.
6. Select your repo destination.

### **GitHub Desktop**

1. On GitHub Desktop, click `Clone a repository from the internet...`.
2. Click the `URL` tab.
3. At the [azure repository](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation),
under `Repos > Files > FSPDocumentation`, select the `Clone`
button in the top right corner.
4. Copy the `HTTPS` link and paste it in the `URL` field on GitHub Desktop.
5. Change your local path is desired.
6. Click `Clone`.

### **Command Line**

```cmd
<!-- data-prompt="C:\..\repos>" -->
git clone https://fullsailpartners@dev.azure.com/fullsailpartners/FSPDocumentation/_git/FSPDocumentation
```

<!-- tabs:end -->

## Creating A Branch

Branches are used to separate work. A person can have as many
branches as they wish, and each branch should be used for a single
feature/update. For example, a set of pages that relate to the same
topic could all be contained in the same branch, but pages that all
relate to different things should be in different branches.

### Remote vs Local

As mentioned before, a `git` repo makes it easy to keep your local
copy up to date. In order to do so, there is the concept of the repo's
`remote`. The default remote (a location where files
are stored remotely) is automatically added when you cloned the
repo, and this is considered the `origin`.

The `origin` is the sole source of truth for a repository - it's the
place where code is shared between all members of the repository.
This makes it easy to do two things:

1. Update your local copy of the repository with changes made by others
2. Upload your changes and share them with others.

> TFS holds all files in a single directory, and you checkout the
> specific files you want to work with before uploading them back
> to TFS. Git works much the same, except instead of individual files,
> you create a personal copy of the project and make changes to that.
> This allows for much easier conflict resolution when multiple people
> change the same files.

When you clone the repository, you also get a local copy of the default
branch. In order to make changes, you need to make a new branch first.
It is highly recommended to create branches off remote branches, and
not your local copy. You may forget to update your local copy first, so
creating a branch from an `origin/<branch name>` makes your life
easier in the long run.

*As a note: the term `checkout` (checkout a branch) means you switch to
it locally. Checking out a branch can be used when talking about switching
between branches locally or downloading a new local copy of a remote
branch.*

#### Creating And Using Branches

<!-- tabs:start -->

### **vscode**

1. Go to the source control menu (`ctrl+shift+g`).
2. Click the refresh button near the top left.
3. Click the three dots (`...`) near the top left.
4. Under branch, select `Create branch from...`.
5. Select the branch you want to create a new branch from. This will usually be `origin/main`.
6. Give your new branch a name. The name should be descriptive of
the changes being made.

If you wish to checkout an existing branch, just select the branch name
of the existing branch in step 3.

### **GitHub Desktop**

!> GitHub Desktop does not let you make branches from `origin/main`. Make
sure your branch has been pulled from `origin` before making a new branch.

1. Click the Current Branch dropdown.
2. Select `new branch`.
3. Give the branch a name.

If you wish to checkout an existing branch, select the branch from the menu
that appears in step 1 *instead of* going to step 2.

### **Command Line**

```cmd
git fetch
git switch origin/main
```

A list of remotes can be seen with:

```cmd
git branch -v -a
```

<!-- tabs:end -->

When checking out a branch in this manner, git automatically keeps
track of which remote branch your local branch is a copy of (if
there is no remote, one will be created on the first push). This
allows you to make changes to that branch without having to
select the remote branch every time.

To switch/checkout branches, its quite simple:

<!-- tabs:start -->

### **vscode**

1. Select the branch name in the bottom left corner
2. Select a different branch

Or:

1. Go to the source control menu (`ctrl+shift+g`).
2. Click the three dots (`...`) near the top left.
3. Click `Checkout to...`.
4. Select a different branch.

### **GitHub Desktop**

1. Click current branch
2. Select a different branch

### **Command Line**

```cmd
git switch my-other-branch
```

<!-- tabs:end -->

## Making Changes (Commits)

A `commit` is a change to a branch on a repository. Each commit
can contain multiple files, but should usually be a "single change."
For example, if you were updating the contribution docs, making
a change to this page would be one commit, and making a change to
the CONTRIBUTING page would be a different commit.

Committing work is how you make changes to a repository. Once
committed, those changes can then by synced to a remote, such
as `orign`. Getting commits from the remote is called a `pull`,
and sending commits to the remote is called a `push`. A `sync`
operation usually involves a `pull` then `push`, with any
conflicts resolved before the `push`.

!> Any commits made on a local copy of the `main` branch
will not be allowed to be pushed to the `remote`. You *must*
use a branch for your work.

<!-- tabs:start -->

### **vscode**

1. Make some changes to the files in your branch.
2. Go to the source control menu (`ctrl+shift+g`).
3. Enter a commit message in the text box at the top left.
4. Click `Commit`
5. Click `Sync Changes` to push your changes to the remote branch.
If the remote does not exist, click `Publish Branch`.

You will probably get a prompt asking you if you want
to commit all changes. This is normal, and you can
respond as you wish.

If you respond with `no` and you want to only commit a few
files rather than all of them, click the `+` icon next
to the file or folder in the changes list. This
`stages` them and allows you to commit just those files.

The other files can then be added as a different commit.

Other notes:

- There is a revert/discard button. This removes any non-committed
changes. It can even restore deleted files.
- You can right click a file and select `Open Changes` to see
a comparison of the pre and post-change state.

### **GitHub Desktop**

1. Make some changes to the files in your branch.
2. In GitHub desktop, the list of files that has been changed
default to checked. This means they will be included in the
commit. Deselect any you don't want in your commit (if applicable).
3. Enter a commit summary in the bottom left. A description is optional.
4. Click `commit to <branch name>`.
5. Click `Push to origin`.

Other notes:

- The file display shows the changes made to a file from the previous commit.

### **Command Line**

```cmd
git add *
git commit -m "Adding all commits"
git pull
git push
```

<!-- tabs:end -->

## Pull Requests

> Making a pull request will always be done from the
[Remote Repo](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation).

A pull request (PR) is a request made to take the changes you have made
on your branch and merge them with a different branch. This is usually
done with an update branch to the `main` branch, but it can be used on
any branches.

In order to start a pull request, head over to the [Azure Repo](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation).

1. Click `Pull Requests` from the left menu (under `Repos`).
2. Click `New pull request` in the top right.
3. At the top, select the branch you want to merge and the branch you
want it to merge into. Ex: `my-fancy-update` into `main`.
4. Give the PR a title.
5. Give the PR a description. This should include a summary of
all your changes, any backlog items resolved, etc.
6. Give it a `public` and/or `internal` tag depending on what
type of documentation was updated.
7. Click `Create`. In the dropdown on the create button, you
can click `Create as draft` to make a draft PR.

## Merge Conflicts and Rebasing

Sometimes, merging two branches will result in conflicts between the files.
This can happen because multiple people edit the same files and these
changes come into conflict with each other, or files that were removed
in one branch were updated in another, etc.

When this happens, `git` will try and reconcile the changes automatically.
This does not always work, and you will need to perform a `rebase` operation
to make sure your branch is ready to merge.

It's good practice to do a `rebase` off your target branch anyways, but its
not always needed, especially if you know for sure your changes are the
only ones.

A `rebase` is the act of catching one branch up with the changes on another
branch. In this case, you would `rebase` your update branch onto `main`.
When there are file conflicts, applications `vscode` and `Visual Studio`
give you a way interactively modify the contents of files.
Regular text editors work as well, but those require manual editing
of files to either keep or remove offending conflicts.

You can set the tool used for resolving merge conflicts via the
command line: `git config --global merge.tool <tool-name>`. This
was set during your install of `git`, but you can change the tool
specifically for merges. To use `vscode`, for example, run:

```cmd
<!-- data-prompt="C:\User\Aria>" -->
git config --global merge.tool code
```

<!-- tabs:start -->

### **vscode**

> In order to do a local rebase, you must be abel to force push.
> You can enable this by going to the gear in the bottom left and
> clicking `settings` (`ctrl+,`). Search for `Force Push` and enable
> the `Git: Allow Force Push` setting.

1. Checkout your working branch (switch to it).
2. Click the three dots (`...`) near the top left.
3. Click `Branch > Rebase`.
4. Select the branch to rebase to. ex: `origin/main`.
5. Wait for the operation to start.
   1. If you have merge conflicts, select the file from the left menu.
   2. In the file, you will see sections of text labeled as from
   local and from head. (head is your remote branch).
   3. Modify your files so the documents are correct by keeping the
   local copy, keeping the remote copy, or combining the two
   (you have to do that by hand). Make sure to delete all merge
   artifacts (like: `>>>>>` or `<<<<<` or `-----`).
6. Continue the operation until there are no conflicts.
7. Click the three dots (`...`) near the top left.
8. Select `Push > Force Push`.
9. Confirm the dialogue box if necessary.

<!-- tabs:end -->
