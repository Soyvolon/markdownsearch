<!-- docs/markdown.md -->

# Markdown

Markdown is a lightweight markup language that is used to easily
create formatted text and is especially useful for publishing
documents on the internet (like this one).

All markdown files end in `.md`, and all documents that are used for
this documentation website are markdown files.

## **Basic Markdown Syntax**

- **Headings**: Use `#` characters to denote headings (e.g., `# Introduction`).
- **Paragraphs**: Separate paragraphs with a blank line.
- **Lists**: Create ordered lists with numbers (`1. Item 1`)
or unordered lists with asterisks (`* Item A`).
- **Emphasis**: Use asterisks or underscores for *italic*
(`*italic*`) and double asterisks or underscores for **bold** (`**bold**`).
- **Links**: Add links using `[link text](link)` (e.g., `[Google](https://google.com)`).

For a more in depth explanation on markdown, read [this article](https://www.markdownguide.org/getting-started/).
You can also access the other pages on this site to view a
markdown cheat sheet, syntax guide, and advanced syntax information.

If you have a question like: "How do I make a table in markdown?" or
"How do I do \<insert thing here> in markdown?" - google it! The answer
is probably already out there.
