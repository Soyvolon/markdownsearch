﻿using MarkdownSearch.Data.Engine;
using MarkdownSearch.Services;
using MarkdownSearch.Test.Data;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownSearch.Test;

[TestClass]
public class AltEngineTest
{
#nullable disable
    private const string LinkStart = "https://docs.visionhosted.com/public";
    private IMarkdownSearchService<ConstFolderEngine, ConstPageEngine> _service;
#nullable enable

    [TestInitialize]
    public void Initialize()
    {
        _service = new MarkdownSearchService<ConstFolderEngine, ConstPageEngine>();
        _service.SetLinkStart(LinkStart);
    }

    [TestMethod]
    public async Task TestInheritanceMethodsAsync()
    {
        var c = new Counter();
        _service.SetFolderParams(c);

        var res = await _service.SearchFolders(["/"], "ahh");

        Assert.IsNotNull(res);
    }

    [TestCleanup]
    public void Cleanup()
    {
        _service.Dispose();
    }
}
