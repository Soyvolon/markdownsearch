﻿using MarkdownSearch.Data.Engine;

using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownSearch.Test.Data;

public class Counter
{
    public int Count { get; set; } = 2;
}

public class ConstFolderEngine(string path, Counter counter)
        : FolderEngine<ConstPageEngine>(path)
{
    protected override Task<string[]> GetChildFoldersAsync()
    {
        if (counter.Count-- <= 0)
            return Task.FromResult<string[]>([]);

        return Task.FromResult<string[]>(["one", "two"]);
    }

    protected override Task<string[]> GetChildPagesAsync()
    {
        return Task.FromResult<string[]>([]);
    }
}

public class ConstPageEngine(
        string path, 
        string searchRoot, 
        string prefix
    ) : PageEngine(path, searchRoot, prefix)
{
    private static readonly string[] items = [
        @"# title

Some other text.",

        @"# nope

This is a nope.",

        @"# ahh

Ahhhhh"
    ];

    private static readonly Random _random = new();

    protected override Task<string> GetContentsAsync()
    {
        int index = _random.Next(items.Length);
        return Task.FromResult(items[index]);
    }
}
