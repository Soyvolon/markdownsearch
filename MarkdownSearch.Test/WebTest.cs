﻿using MarkdownSearch.Data.Engine;
using MarkdownSearch.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace MarkdownSearch.Test;

[TestClass]
public class WebTest
{
#nullable disable
    private const string LinkStart = "https://docs.visionhosted.com/public";
    private IMarkdownSearchService<FolderEngine<PageEngine>, PageEngine> _service;
#nullable enable

    [TestInitialize]
    public void Initialize()
    {
        _service = new MarkdownSearchService<FolderEngine<PageEngine>, PageEngine>();
        _service.SetLinkStart(LinkStart);
    }

    [TestMethod]
    [DataRow(
        new string[] { "res/search" },
        new string[] { "git" }
    )]
    public async Task SerializationTest(string[] folders, string[] terms)
    {
        var res = await _service.SearchFolders(folders, terms);

        var json = JsonSerializer.Serialize(res);

        Assert.IsNotNull(json);
    }

    [TestCleanup]
    public void Cleanup()
    {
        _service = null!;
    }
}
