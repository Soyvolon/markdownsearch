﻿using MarkdownSearch.Data.Engine;
using MarkdownSearch.Services;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MarkdownSearch.Test;

[TestClass]
public class DefaultSearchTest
{
#nullable disable
    private const string LinkStart = "https://docs.visionhosted.com/public";
    private IMarkdownSearchService<FolderEngine<PageEngine>, PageEngine> _service;
#nullable enable

    [TestInitialize]
    public void Initialize()
    {
        _service = new MarkdownSearchService<FolderEngine<PageEngine>, PageEngine>();
        _service.SetLinkStart(LinkStart);
    }

    [TestMethod]
    [DataRow(
        new string[] {"res/search"},
        new string[] {"git"},
        new string[] {"README.md#contributing", "README.md#resources"}
    )]
    public async Task TestDefaultSearchLinksAsync(string[] folders, string[] terms, 
        string[] relLinks)
    {
        var res = await _service.SearchFolders(folders.Select(Path.GetFullPath), terms);

        Assert.AreEqual(relLinks.Length, res.Results.Count, "Result count mismatch.");

        List<string> rel = new(relLinks);
        foreach(var r in res.Results)
        {
            Assert.IsNotNull(r.Link, "Link not set.");
            Assert.IsTrue(r.Link.AbsoluteUri.StartsWith(LinkStart, StringComparison.OrdinalIgnoreCase),
                $"Link does not start with {nameof(LinkStart)}.");

            var i = rel.IndexOf(r.RelativeLink);
            Assert.AreNotEqual(-1, i, "Relative link not found in results.");

            rel.RemoveAt(i);
        }

        Assert.AreEqual(0, rel.Count, "Leftover relative links remaining.");
    }

    [TestMethod]
    [DataRow(
        new string[] { "res/search" },
        new string[] { "git" },
        new string[] { "Contributing", "Resources" }
    )]
    public async Task TestDefaultSearchTitlesAsync(string[] folders, string[] terms,
        string[] titlesToCheck)
    {
        var res = await _service.SearchFolders(folders.Select(Path.GetFullPath), terms);

        Assert.AreEqual(titlesToCheck.Length, res.Results.Count, "Result count mismatch.");

        List<string> titles = new(titlesToCheck);
        foreach (var r in res.Results)
        {
            var i = titles.IndexOf(r.Title);
            Assert.AreNotEqual(-1, i, "Title not found in results.");

            titles.RemoveAt(i);
        }

        Assert.AreEqual(0, titles.Count, "Leftover titles remaining.");
    }

    [TestMethod]
    [DataRow(
        new string[] { "res/search" },
        new string[] { "dog" },
        new string[] { "git" },
        new string[] { "dog/README.md#contributing", "dog/README.md#resources" }
    )]
    public async Task TestDefaultSearchPrefixesAsync(string[] folders, string[] prefixes,
        string[] terms,
        string[] linksToCheck)
    {
        Assert.AreEqual(prefixes.Length, folders.Length, "Folders and prefixes must be the same length.");

        var res = await _service.SearchFolders(folders.Select(Path.GetFullPath), prefixes, terms);

        Assert.AreEqual(linksToCheck.Length, res.Results.Count, "Result count mismatch.");

        List<string> links = linksToCheck.Select(e => LinkStart + '/' + e).ToList();
        List<string> relLinks = new(linksToCheck);
        foreach (var r in res.Results)
        {
            var i = relLinks.IndexOf(r.RelativeLink);
            Assert.AreNotEqual(-1, i, "Relative link not found in results.");

            relLinks.RemoveAt(i);

            i = links.IndexOf(r.Link?.AbsoluteUri ?? "");
            Assert.AreNotEqual(-1, i, "Absolute link not found in results.");

            links.RemoveAt(i);
        }

        Assert.AreEqual(0, links.Count, "Leftover absolute links remaining.");
        Assert.AreEqual(0, relLinks.Count, "Leftover relative links remaining.");
    }

    [TestMethod]
    [DataRow(
        new string[] { "res/search" },
        new string[] { "git" },
        new string[] { @"## Contributing

For information on contributing to FullSail Partners' documentation,
please visit the [Contributing](/docs/CONTRIBUTING.md) page.

Looking for a refresher on how to utilize `git` for this repo?
Check out the [Git](/docs/git.md) page.",
        @"## Resources

- The [FullSail Partners Documentation Repository](https://dev.azure.com/fullsailpartners/_git/FSPDocumentation)."}
    )]
    public async Task TestDefaultSearchContentsAsync(string[] folders, string[] terms,
        string[] contentsToCheck)
    {
        var res = await _service.SearchFolders(folders.Select(Path.GetFullPath), terms);

        Assert.AreEqual(contentsToCheck.Length, res.Results.Count, "Result count mismatch.");

        List<string> contents = new(contentsToCheck);
        foreach (var r in res.Results)
        {
            var i = contents.IndexOf(r.Contents.Trim());
            Assert.AreNotEqual(-1, i, "Contents not found in results.");

            contents.RemoveAt(i);
        }

        Assert.AreEqual(0, contents.Count, "Leftover contents remaining.");
    }

    [TestCleanup]
    public void Cleanup()
    {
        _service = null!;
    }
}
