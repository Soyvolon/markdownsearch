using static System.Net.Mime.MediaTypeNames;
using System.Reflection.Emit;
using MarkdownSearch.Data.Engine;

namespace MarkdownSearch.Test;

[TestClass]
public class EngineTest
{
    [TestMethod]
    [DataRow("res/test-file.md",
        new string[] { 
            "level", 
            //"1", 
            "some", 
            "other", 
            "text", 
            //"in", 
            //"2", 
            //"3", 
            //"2b", 
            //"2c", 
            "vscode", 
            //"2d", 
            //"4"
        }
    )]
    public async Task TestFileEngineKeywordList(string file, string[] keywords)
    {
        Assert.IsTrue(File.Exists(file), $"Test file ({file}) missing.");

        var fileEngine = new PageEngine(file, "res", "");
        await fileEngine.InitializeAsync();

        List<string> tests = new(keywords);
        List<string> actual = new(fileEngine.Keywords);

        Assert.AreEqual(tests.Count, actual.Count, "Keyword list sizes are not the same.");

        while (tests.Count > 0)
        {
            var test = tests[0];
            var index = actual.IndexOf(test);

            if (index > -1)
                actual.RemoveAt(index);

            tests.RemoveAt(0);
        }

        Assert.AreEqual(tests.Count, actual.Count, "Keyword list sizes are not the same.");
    }

    [TestMethod]
    [DataRow("res/test-file.md",
        new string[] { "", "Level 1", "Level 2", "Level 3", "Level 2b", "Level 2c", "**vscode**", 
                "Level 4", "Level 2d", "**vscode**"},
        new int[] { -1, 1, 2, 3, 2, 2, 3, 4, 2, 3 }
    )]
    public async Task TestFileEngineHeaders(string file, string[] titles, int[] depths)
    {
        Assert.IsTrue(File.Exists(file), $"Test file ({file}) missing.");
        Assert.AreEqual(titles.Length, depths.Length, "Test array lengths do not match.");

        var fileEngine = new PageEngine(file, "res", "");
        await fileEngine.InitializeAsync();

        Stack<MarkdownHeader> headers = [];
        headers.Push(fileEngine.RootHeader);

        int c = 0;
        while(headers.TryPop(out var head))
        {
            Assert.AreEqual(titles[c], head.Name, "Bad header name.");
            Assert.AreEqual(depths[c], head.Depth, "Bad header depth.");

            c++;

            // we reverse this due to the nature
            // of the stack we use for testing.
            foreach (var h in head.ChildHeaders
                .AsEnumerable()
                .Reverse())
            {
                headers.Push(h);
            }
        }
    }

    // disabled due to line endings being different on different
    // platforms. See the search test for contents, that does the same
    // thing just on a larger scale.
    //[TestMethod]
    [DataRow("res/test-file.md",
        new string[] { "", "Level 1", "Level 2", "Level 3", "Level 2b", "Level 2c", "**vscode**",
                "Level 4", "Level 2d", "**vscode**"},
        new int[] { 0, 0, 44, 89, 135, 182, 229, 277, 324, 371 },
        // where -1 is equal to null
        new int[] { -1, -1, 135, 135, 182, 324, 324, 324, -1, -1 }
    )]
    public async Task TestFileEngineIndexing(string file, string[] titles, int[] starts, int[] ends)
    {
        Assert.IsTrue(File.Exists(file), $"Test file ({file}) missing.");
        Assert.AreEqual(titles.Length, starts.Length, "Test array (starts) lengths do not match.");
        Assert.AreEqual(titles.Length, ends.Length, "Test array (ends) lengths do not match.");

        var fileEngine = new PageEngine(file, "res", "");
        await fileEngine.InitializeAsync();

        Stack<MarkdownHeader> headers = [];
        headers.Push(fileEngine.RootHeader);

        int c = 0;
        while (headers.TryPop(out var head))
        {
            Assert.AreEqual(titles[c], head.Name, "Bad header name.");
            Assert.AreEqual(starts[c], head.Start, "Bad header start.");
            Assert.AreEqual(ends[c] == -1 ? null : ends[c], head.End, "Bad header end.");

            c++;

            // we reverse this due to the nature
            // of the stack we use for testing.
            foreach (var h in head.ChildHeaders
                .AsEnumerable()
                .Reverse())
            {
                headers.Push(h);
            }
        }
    }

    [TestMethod]
    [DataRow("res/test-file.md", new string[] { "text", "level 1", "singing" }, false, true)]
    [DataRow("res/test-file.md", new string[] { "other", "text" }, true, true)]
    [DataRow("res/test-file.md", new string[] { "singing" }, false, false)]
    [DataRow("res/test-file.md", new string[] { "text", "level", "singing" }, true, false)]
    public async Task TestHasKeyword(string file, string[] terms, bool matchAll, bool result)
    {
        Assert.IsTrue(File.Exists(file), $"Test file ({file}) missing.");

        var fileEngine = new PageEngine(file, "res", "");
        await fileEngine.InitializeAsync();

        var hasKeyword = fileEngine.HasKeyword(matchAll, terms);

        Assert.AreEqual(result, hasKeyword, "Has Keyword did not have the expected result.");
    }

    [TestMethod]
    [DataRow("res/data-folder", 7, 0)]
    public async Task TestFolderParseEngineInitialization(string folder, int files, int folders)
    {
        Assert.IsTrue(Directory.Exists(folder), $"Test folder ({folder}) missing.");
        Assert.IsTrue(Directory.GetFileSystemEntries(folder).Length > 0, 
            $"Test folder ({folder}) is empty.");

        var folderEngine = new FolderEngine<PageEngine>(folder);
        await folderEngine.InitializeAsync("res", "");

        Assert.AreEqual(files, folderEngine.Pages.Count, "File count does not match expected.");
        Assert.AreEqual(folders, folderEngine.Folders.Count, "Folder count does not match expected.");
    }
}