# MarkdownSearch

A tool to search a folder-like structure of markdown data (such as those used when making a 
[docsify](https://docsify.js.org/#/) site) for a query. The results are designed to be 
used as a response to ML machines, so it includes the contents of the heading that contains 
the search terms in the result.

## Usage

Create a new instance of `MarkdownSearchService` in the `MarkdownSearch.Services` namespace.
Or, add it to dependency injection using the `IMarkdownService` interface.

You can view the [DefaultSearchTests.cs](https://gitlab.com/Soyvolon/markdownsearch/-/blob/master/MarkdownSearch.Test/DefaultSearchTest.cs?ref_type=heads)
file for more usage cases.

The search can be configured using the `SearchOptions` class.
