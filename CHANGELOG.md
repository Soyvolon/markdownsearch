# 2.0.0

+ Updated engines to use abstract types and activators 
to allow for custom implementation of how an engine gets child data.
+ Updated service to require a type definition on creation.

# 1.1.0

+ Downgraded code version to a framework for better compat

# 1.0.0

+ Added services, file parse engines, and initial search capabilities
